
--
-- Base de datos: `PRUEBAS_MYSQL`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UAIN_CATEGORIAS`
--

CREATE TABLE `UAIN_CATEGORIAS` (
  `UAIN_ID_CAT` int(11) NOT NULL,
  `UAIN_CATEGORIA` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UAIN_CLIENTES`
--
---
CREATE TABLE `UAIN_CLIENTES` (
  `UAIN_ID_CLI` int(11) NOT NULL,
  `UAIN_NOM` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `UAIN_APE` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `UAIN_CEL` int(9) NOT NULL,
  `UAIN_PAIS` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `UAIN_EMAIL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UAIN_FECH_NAC` date NOT NULL,
  `UAIN_RUC` int(11) DEFAULT NULL,
  `UAIN_ID_TPC` int(11) NOT NULL,
  `UAIN_ID_USU` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UAIN_PRODUCTO`
--

CREATE TABLE `UAIN_PRODUCTO` (
  `UAIN_ID_PROD` int(11) NOT NULL,
  `UAIN_PROD` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `UAIN_PRECIO` decimal(4,4) NOT NULL,
  `UAIN_ID_CAT` int(11) NOT NULL,
  `UAIN_ID_PROV` int(11) NOT NULL,
  `UAIN_MARCA` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UAIN_PROVEEDORES`
--

CREATE TABLE `UAIN_PROVEEDORES` (
  `UAIN_ID_PROV` int(11) NOT NULL,
  `UAIN_RAZON_SOCIAL` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `UAIN_TELEFONO` int(7) DEFAULT NULL,
  `UAIN_SECTOR` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UAIN_DIR` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UAIN_TP_CLIENTE`
--

CREATE TABLE `UAIN_TP_CLIENTE` (
  `UAIN_ID_TPC` int(11) NOT NULL,
  `UAIN_TIPO` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UAIN_USUARIOS`
--

CREATE TABLE `UAIN_USUARIOS` (
  `UAIN_ID_USU` int(11) NOT NULL,
  `UAIN_USU` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `UAIN_PASS` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `UAIN_CATEGORIAS`
--
ALTER TABLE `UAIN_CATEGORIAS`
  ADD PRIMARY KEY (`UAIN_ID_CAT`);

--
-- Indices de la tabla `UAIN_CLIENTES`
--
ALTER TABLE `UAIN_CLIENTES`
  ADD PRIMARY KEY (`UAIN_ID_CLI`),
  ADD KEY `FK_CLI` (`UAIN_ID_TPC`) USING BTREE,
  ADD KEY `FK_USU` (`UAIN_ID_USU`);

--
-- Indices de la tabla `UAIN_PRODUCTO`
--
ALTER TABLE `UAIN_PRODUCTO`
  ADD PRIMARY KEY (`UAIN_ID_PROD`),
  ADD KEY `FK_PROV` (`UAIN_ID_PROV`),
  ADD KEY `FK_CAT` (`UAIN_ID_CAT`);

--
-- Indices de la tabla `UAIN_PROVEEDORES`
--
ALTER TABLE `UAIN_PROVEEDORES`
  ADD PRIMARY KEY (`UAIN_ID_PROV`);

--
-- Indices de la tabla `UAIN_TP_CLIENTE`
--
ALTER TABLE `UAIN_TP_CLIENTE`
  ADD PRIMARY KEY (`UAIN_ID_TPC`);

--
-- Indices de la tabla `UAIN_USUARIOS`
--
ALTER TABLE `UAIN_USUARIOS`
  ADD PRIMARY KEY (`UAIN_ID_USU`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `UAIN_CATEGORIAS`
--
ALTER TABLE `UAIN_CATEGORIAS`
  MODIFY `UAIN_ID_CAT` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `UAIN_CLIENTES`
--
ALTER TABLE `UAIN_CLIENTES`
  MODIFY `UAIN_ID_CLI` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `UAIN_PRODUCTO`
--
ALTER TABLE `UAIN_PRODUCTO`
  MODIFY `UAIN_ID_PROD` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `UAIN_PROVEEDORES`
--
ALTER TABLE `UAIN_PROVEEDORES`
  MODIFY `UAIN_ID_PROV` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `UAIN_TP_CLIENTE`
--
ALTER TABLE `UAIN_TP_CLIENTE`
  MODIFY `UAIN_ID_TPC` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `UAIN_USUARIOS`
--
ALTER TABLE `UAIN_USUARIOS`
  MODIFY `UAIN_ID_USU` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `UAIN_CLIENTES`
--
ALTER TABLE `UAIN_CLIENTES`
  ADD CONSTRAINT `FK_CLI` FOREIGN KEY (`UAIN_ID_TPC`) REFERENCES `UAIN_TP_CLIENTE` (`uain_id_tpc`),
  ADD CONSTRAINT `FK_USU` FOREIGN KEY (`UAIN_ID_USU`) REFERENCES `UAIN_USUARIOS` (`uain_id_usu`);

--
-- Filtros para la tabla `UAIN_PRODUCTO`
--
ALTER TABLE `UAIN_PRODUCTO`
  ADD CONSTRAINT `FK_CAT` FOREIGN KEY (`UAIN_ID_CAT`) REFERENCES `UAIN_CATEGORIAS` (`uain_id_cat`),
  ADD CONSTRAINT `FK_PROV` FOREIGN KEY (`UAIN_ID_PROV`) REFERENCES `UAIN_PROVEEDORES` (`uain_id_prov`);
COMMIT;

